---
layout: markdown_page
title: "IT Ops"
---
# Welcome to the IT Ops Handbook

The IT Ops department is part of the [GitLab Business Ops](/handbook/business-ops) function that guides systems, workflows and processes and is a singular reference point for operational management. 

## GET IN TOUCH

* [IT Ops Issues](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues)
* #it-ops on slack

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement

IT Ops will work with Security, PeopleOps and Business Operations to develop automated onboarding and offboarding processes. We will develop secure integrations between Enterprise Business Systems and with our Data Lake. We will develop tooling and process to facilitate end-user asset management, provisioning and tracking. We will work to build API Integrations from the HRIS to third party systems and GitLab.com. We triage all IT related questions as they arise. We build and maintain cross-functional relationships with internal teams to champion initiatives. We will spearhead onboarding and offboarding automation efforts with a variety of custom API integrations, including GitLab.com and third-party resources, not limited to our tech-stack, with scalability in mind.


## Access requests

For requests for access to a system or application you do not currently have, or need admin access to, please go to [access requests](
https://gitlab.com/gitlab-com/access-requests). You can also resquest new slack groups and channels and new Google groups and aliases here as well. In addition, you can submit an issue for access reviews. 

## Two factor issues and requests for support 

Submit requests for support via [email](https://docs.google.com/document/d/19v2pVWhq_BStUzIY-_p7zPNVAGjRBwS6SHOWtDdFtfA/edit?usp=sharing). Alternatively you can submit an issue directly to [It Ops](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues). Screenshots and videos are very helpful when experincing an issue, especially if there is an error message. 


## Laptop Management

Latops are purchased by IT Ops when a new employee comes on board. They will be sent a form to fill out for ordering. If you are an exisitng employee and your laptop is broken or at end of life (EOL is 3 years+) you may request a new one. Please submit an issue in IT Ops issues and select the [repair_replace](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues) template.

## Other Resources

### Okta 

In an effort to secure access to systems, GitLab is utilizing Okta. The key goals are:

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorised connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and Deprovisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.

To read more about Okta, please visit the [Okta](https://about.gitlab.com/handbook/business-ops/okta/) pageof the handbook. 




