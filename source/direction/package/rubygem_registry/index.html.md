---
layout: markdown_page
title: "Category Vision - RubyGem Registry"
---

- TOC
{:toc}

## RubyGem Registry
A Rubygem registry offers ruby developers of lower-level services to provision an easy to use, integrated solution to share and version control ruby gems in a standardized and controlled way. Being able to provision them internally sets projects up for improved privacy and pipeline build speeds.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Rubygem%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## What's Next & Why

TODO

## Competitive Landscape

TODO

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
