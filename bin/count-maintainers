#!/usr/bin/env ruby

require 'docopt'
require 'date'
require 'yaml'

docstring = <<DOCSTRING
Show historical stats on maintainers of GitLab

Usage:
  #{__FILE__}
  #{__FILE__} -h | --help

Options:
  -h --help                  Show this screen.
DOCSTRING

def maintainer_count(team_page, type)
  if team_page.first['maintains']
    maintains = "#{type} merge request"

    # We used to call them endbosses.
    look_for = ['endboss']

    # Then we added frontend-specific endbosses.
    look_for << maintains if team_page.any? do |person|
      person["maintains"] && person["maintains"].include?(maintains)
    end

    team_page.select do |person|
      maintains = person['maintains']

      maintains && look_for.all? { |s| maintains.include?(s) }
    end.count
  else
    team_page.select do |person|
      person['projects'] &&
        person['projects']['gitlab-ce'] == "maintainer #{type.downcase}"
    end.count
  end
end

def backend_developer_count(team_page)
  team_page.select do |person|
    person['gitlab'] && # Exclude vacancies
      (person['role'].match(/Developer[^<]*</) ||
       person['role'].include?('Backend Engineer') ||
       person['role'].include?('Engineering Fellow'))
  end.count
end

def frontend_developer_count(team_page)
  team_page.select do |person|
    person['gitlab'] && # Exclude vacancies
      person['role'].include?('Frontend Engineer') ||
      person['role'].include?('Frontend Lead')
  end.count
end

def format_row(date, maintainer_count, developer_count)
  row = [date, maintainer_count, developer_count, (developer_count.to_f / maintainer_count).round(1)]
  "| #{row.join(' | ')} |"
end

def counts_on(date)
  team_page_text = `git show $(git log --before=#{date} -1 --pretty='%H'):data/team.yml`
  team_page = YAML.load(team_page_text)

  backend_counts = {
    maintainer: maintainer_count(team_page, 'Backend'),
    developer: backend_developer_count(team_page)
  }

  # Before we were not differentiating between FE and backend
  if date >= Date.new(2017, 1, 1)
    frontend_counts = {
      maintainer: maintainer_count(team_page, 'Frontend'),
      developer: frontend_developer_count(team_page)
    }
  end

  [backend_counts, frontend_counts]
end

def show_maintainers
  current_date = Date.new(2016, 7, 1)
  end_date = Date.today
  backend = []
  frontend = []

  header = [
    '| Date | Maintainers | Developers | Developers per maintainer |',
    '| --- | --- | --- | --- |'
  ]

  loop do
    current_date = [current_date, end_date].min
    backend_counts, frontend_counts = counts_on(current_date)

    backend << format_row(current_date, backend_counts[:maintainer], backend_counts[:developer])

    if frontend_counts
      frontend << format_row(current_date, frontend_counts[:maintainer], frontend_counts[:developer])
    end

    break if current_date >= end_date

    current_date = current_date >> 6
  end

  puts "Backend:\n"
  puts header.join "\n"
  puts backend.join "\n"
  puts "\nFrontend:"
  puts header.join "\n"
  puts frontend.join "\n"
end

begin
  options = Docopt::docopt(docstring)

  show_maintainers
rescue Docopt::Exit => e
  puts e.message
end
